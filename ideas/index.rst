:html_theme.sidebar_primary.remove: true
:sd_hide_title: true

.. _gsoc-project-ideas:

Ideas
######

.. image:: ../_static/images/ideas-below.webp
   :align: center

.. admonition:: How to participate?

   Contributors are expected to go through the list of ideas dropdown below and join the discussion by clicking on the
   ``Discuss on forum`` button. All ideas have colorful badges for ``Complexity`` and
   ``Size`` for making the selection process easier for contributors. Anyone is welcome to
   introduce new ideas via the `forum gsoc-ideas tag <https://forum.beagleboard.org/tag/gsoc-ideas>`_.
   Only ideas with sufficiently experienced mentor backing as deemed by the administrators will
   be added here.

   +------------------------------------+-------------------------------+
   | Complexity                         | Size                          |
   +====================================+===============================+
   | :bdg-danger:`High complexity`      | :bdg-danger-line:`350 hours`  |
   +------------------------------------+-------------------------------+
   | :bdg-success:`Medium complexity`   | :bdg-success-line:`175 hours` |
   +------------------------------------+-------------------------------+
   | :bdg-info:`Low complexity`         | :bdg-info-line:`90 hours`     |
   +------------------------------------+-------------------------------+

.. tip::
   
   Below are the latest project ideas, you can also check our our :ref:`gsoc-old-ideas` and :ref:`Past_Projects` for inspiration.

.. card:: A Conversational AI Assistant for BeagleBoard using RAG and Fine-tuning

   :fas:`brain;pst-color-secondary` Deep Learning :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`

   ^^^^

   BeagleBoard currently lacks an AI-powered assistant to help users troubleshoot errors. This project aims to address that need while also streamlining the onboarding process for new contributors, enabling them to get started more quickly.

   | **Goal:** Develop a domain-specific chatbot for BeagleBoard using a combination of RAG and fine-tuning of an open-source LLM (like Llama 3, Mixtral, or Gemma).  This chatbot will assist users with troubleshooting, provide information about BeagleBoard products, and streamline the onboarding process for new contributors.
   | **Hardware Skills:** Ability to test applications on BeagleBone AI-64/BeagleY-AI and optimize for performance using quantization techniques.
   | **Software Skills:** Python, RAG, Scraping techniques, Fine tuning LLMs, Gradio, Hugging Face Inference Endpoints, NLTK/spaCy, Git
   | **Possible Mentors:** `Aryan Nanda <https://forum.beagleboard.org/u/aryan_nanda/>`_
   
   ++++

   .. button-link:: https://forum.beagleboard.org/t/beaglemind/40806
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: Update beagle-tester for mainline testing

   :fab:`linux;pst-color-primary` Linux kernel improvements :bdg-success:`Medium complexity` :bdg-danger-line:`350 hours`

   ^^^^

   Utilize the ``beagle-tester`` application and ``Buildroot`` along with device-tree and udev symlink concepts within
   the OpenBeagle continuous integration server context to create a regression test suite for the Linux kernel
   and device-tree overlays on various Beagle computers.

   | **Goal:** Execution on Beagle test farm with over 30 mikroBUS boards testing all mikroBUS enabled cape interfaces (PWM, ADC, UART, I2C, SPI, GPIO and interrupt) performing weekly mainline Linux regression verification
   | **Hardware Skills:** `basic wiring`_, `embedded serial interfaces`_
   | **Software Skills:** `device-tree`_, `Linux`_, `C`_, `OpenBeagle CI`_, `Buildroot`_
   | **Possible Mentors:** `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_, `Anuj Deshpande <https://forum.beagleboard.org/u/Anuj_Deshpande>`_, `Dhruva Gole <https://forum.beagleboard.org/u/dhruvag2000>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/update-beagle-tester-for-cape-mikrobus-new-board-and-upstream-testing/37279
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: Upstream wpanusb and bcfserial

   :fab:`linux;pst-color-primary` Linux kernel improvements :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`
   
   ^^^^

   These are the drivers that are used to enable Linux to use a BeagleConnect Freedom as a SubGHz IEEE802.15.4 radio (gateway).
   They need to be part of upstream Linux to simplify on-going support. There are several gaps that are known before they are
   acceptable upstream.

   | **Goal:** Add functional gaps, submit upstream patches for these drivers and respond to feedback
   | **Hardware Skills:** `wireless communications`_
   | **Software Skills:** `C`_, `Linux`_
   | **Possible Mentors:** `Ayush Singh <https://forum.beagleboard.org/u/ayush1325>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/upstream-wpanusb-and-bcfserial/37186
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: ``librobotcontrol`` support for newer boards

   :fas:`wand-sparkles;pst-color-danger` Automation and industrial I/O :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`

   ^^^^

   Preliminary librobotcontrol support for BeagleBone AI, BeagleBone AI-64 and BeagleV-Fire has been drafted, but it
   needs to be cleaned up. We can also work on support for Raspberry Pi if UCSD releases their Hat for it.

   | **Goal:** Update librobotcontrol for Robotics Cape on BeagleBone AI, BeagleBone AI-64 and BeagleV-Fire
   | **Hardware Skills:** `basic wiring`_, `motors`_
   | **Software Skills:** `C`_, `Linux`_
   | **Possible Mentors:** `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_
   
   ++++

   .. button-link:: https://forum.beagleboard.org/t/librobotcontrol-support-for-newer-boards/37187
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: Upstream Zephyr Support on BBAI-64 R5

   :fas:`timeline;pst-color-secondary` RTOS/microkernel imporvements :bdg-success:`Medium complexity` :bdg-danger-line:`350 hours`
   
   ^^^^

   Incorporating Zephyr RTOS support onto the Cortex-R5 cores of the TDA4VM SoC along with Linux operation on the A72 core. The objective is to harness the combined capabilities of both systems
   to support BeagleBone AI-64.

   | **Goal:** submit upstream patches to support BeagleBone AI-64 and respond to feedback
   | **Hardware Skills:** Familiarity with ARM Cortex R5
   | **Software Skills:** `C`_, `RTOS  <https://docs.zephyrproject.org/latest/develop/getting_started/index.html>`_
   | **Possible Mentors:** `Dhruva Gole <https://forum.beagleboard.org/u/dhruvag2000>`_, `Nishanth Menon <https://forum.beagleboard.org/u/nishanth_menon>`_
   | **Upstream Repository:** `The primary repository for Zephyr Project <https://github.com/zephyrproject-rtos/zephyr>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/upstream-zephyr-support-on-bbai-64-r5/37294/1
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum





.. button-link:: https://forum.beagleboard.org/tag/gsoc-ideas
   :color: danger
   :expand:
   :outline:

   :fab:`discourse;pst-color-light` Visit our forum to see newer ideas being discussed!

.. toctree::
   :hidden:

   old/index