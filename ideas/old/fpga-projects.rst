.. _gsoc-idea-fpga-projects:

FPGA based projects
####################

.. card:: Low-latency I/O RISC-V CPU core in FPGA fabric

   :fas:`microchip;pst-color-primary` FPGA gateware improvements :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`

   ^^^^

   BeagleV-Fire features RISC-V 64-bit CPU cores and FPGA fabric. In that FPGA fabric, we'd like to
   implement a RISC-V 32-bit CPU core with operations optimized for low-latency GPIO. This is similar
   to the programmable real-time unit (PRU) RISC cores popularized on BeagleBone Black.

   | **Goal:** RISC-V-based CPU on BeagleV-Fire FPGA fabric with GPIO
   | **Hardware Skills:** `Verilog`_, `verification`_, `FPGA`_
   | **Software Skills:** `RISC-V ISA`_, `assembly`_, `Linux`_
   | **Possible Mentors:** `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/low-latency-risc-v-i-o-cpu-core/37156
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum
      
.. card:: 

    :fas:`microchip;pst-color-secondary` **RISC-V Based PRU on FPGA**
    ^^^^

    A programmable real-time unit (PRU) is a fast (200-MHz, 32-bit) processor with single-cycle I/O 
    access to a number of the pins and full access to the internal memory and peripherals on the 
    AM3358 processor on BeagleBones (BeagleBone, BeagleBone Black, BeagleBone Green, etc.). They 
    are designed to provide software-defined peripherals as part of the Programmable Real-time Unit 
    Industrial Control SubSystem (PRU-ICSS) and are capable of implementing things like 25 pulse-width 
    modulators, 4 soft UARTs, stepper motor drivers, and much more. Having these controllers integrated 
    is really handy to avoid throwing in another device to control or interface to a new peripheral. 
    The real power comes when you need high bandwidth between the main CPU and these controllers, 
    such as in LEDscape (https://trmm.net/Category:LEDscape/#LEDscape).

    It would be great to have a RISC-V based PRU running on FPGA which will be interfaced with BBB. 
    Features of PRU like multiple PWM, Soft UARTs, ultra-low latency IO control can be bit-banged on 
    the RV core. Existing cores like Vex, Neorv, serv can be explored for this application.

    - **Goal:** RISC-V Based PRU on FPGA
    - **Hardware Skills:** Verilog, Verification, FPGA
    - **Software Skills:** RISC-V ISA, assembly
    - **Possible Mentors:** Michael Welling, Omkar Bhilare, Kumar Abhishek
    - **Expected Size of Project:** 175 hrs
    - **Rating:** Medium

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-success-line:`Medium size`

.. card:: 

    :fas:`microchip;pst-color-secondary` **Beaglewire Updates**
    ^^^^

    Beaglewire is an FPGA cape for the Beaglebone black that was developed to support a project for 
    the 2017 season of GSoC. This first year of Beaglewire support was very intensive and there some 
    issues were left to be resolved. The most notable known issue is with the SDRAM interface working 
    sporadically. A contributor interested in Verilog, kernel coding and hardware would be an ideal candidate. 
    Each of the subsystems (SPI, PWM, UART, SDRAM, etc) can be tested, corrected if necessary and improved 
    if desired. Furthermore, it may be interesting to implement nMigen support for flexible python 
    based gateware support as a stretch goal.

    - **Goal:** Improve the Gateware/Firmware support for the Beaglewire cape
    - **Hardware Skills:** Schematics, Verilog
    - **Software Skills:** Kernel coding
    - **Possible Mentors:** Michael Welling
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:**
        - https://github.com/mwelling/BeagleWire
        - https://github.com/mwelling/beagle-wire

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`