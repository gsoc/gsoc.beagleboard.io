.. _gsoc-2019-projects:

:far:`calendar-days` 2019
#########################

Clickboard Support Under Greybus
*********************************

.. youtube:: RWBzyHNetOE
   :width: 100%

| **Summary:** Click boards are a flagship hardware product line of MikroElektronika with over 600 add-on boards ranging from wireless connectivity clicks to Human Machine Interface clicks for interfacing with peripheral sensors or transceivers. Most of the Click boards use the common protocols like SPI,I2C or UART to communicate with the Beaglebone and thus the support for them now is accomplished via device tree overlays via the bb.org-overlays repository. 

**Contributor:** Vaishnav M.A.

**Mentors:** Jason Kridner, Ravi Kumar Prasad

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2019/projects/6291016871575552
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2019Proposal/clickboard-support-under-greybus
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

PRU User Space API
*******************

.. youtube:: 3Z2PxDIoCpE
   :width: 100%

| **Summary:** This project aims to provide an API for different programming languages to load firmware, start/stop and communicate with the BeagleBone PRUs (Programmable Realtime Units) from user space using the RemoteProc, RPMsg drivers. Example firmware and user space software are provided to demonstrate the use of the project.

**Contributor:** Pratim Ugale

**Mentors:** Patryk Mężydło, ZeekHuge, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2019/projects/5988515748249600
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2019Proposal/PRUUserSpaceAPI-PratimUgale
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Xen on Beagleboard-x15
***********************

.. youtube:: etbBezZK8-8
   :width: 100%

| **Summary:** The idea behind the project is to make Xen hypervisor available and easy to use on beagleboard-x15. This implementation will allow users to experiment on embedded virtualization and in related fields like automotive, critical systems prototyping and processor's resources sharing.

**Contributor:** Denis Obrezkov

**Mentors:** Julien Grall, Iain Hunter, Hunyue Yau, Stefano Stabellini

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2019/projects/5107782238339072
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2019Proposal/Xen_on_BeagleBoard-x15
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Reference Design For A GPIO-based Parallel Bi-Directional Bus
**************************************************************

.. youtube:: ZZDT6jNslqw
   :width: 100%

| **Summary:** Create a hardware/software design will incorporate shift registers (and potentially logic level converter solutions as appropriate) to allow BB.org hardware to communicate with hardware via a parallel, bi-directional bus. 

**Contributor:** Pranav Kumar

**Mentors:** Andrew Henderson, Hunyue Yau, Kumar Abhishek

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2019/organizations/6713523407683584
         :color: light
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2019Proposal/GPIOParallelBidirComm
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 

   Checkout eLinux page for GSoC 2019 projects `here <https://elinux.org/BeagleBoard/GSoC/2019_Projects>`_ for more details.