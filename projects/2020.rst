.. _gsoc-2020-projects:

:far:`calendar-days` 2020
##########################

PRU Improvements
*****************

.. youtube:: AXacKDdxSdY
   :width: 100%
      
| **Summary:** Programming the PRU is a uphill task for a beginner, since it involves several steps, writing the firmware for the PRU, writing a loader program. This can be a easy task for a experienced developer, but it keeps many creative developers away. So, I propose to implement a REPL based control of the PRU, hiding all the low level things behind the REPL and providing a clean interface to uses PRU.

**Contributor:** Vedant Paranjape

**Mentors:** Abhishek Kumar, Pratim Ugale, Andrew Henderson

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2020/projects/6551712266977280
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2020_Projects/PRU_Improvements
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Cape Compatibility layer for BeagleBone Black and BeagleBone AI
***************************************************************

.. youtube:: jP9fwOxp4Bc
   :width: 100%

| **Summary:** The idea of this project is to make the same user space examples work with both BeagleBone Black and BeagleBone AI, using the same references to drivers for peripherals assigned to the same pins between BeagleBone Black and BeagleBone AI.

**Contributor:** Deepak Khatri

**Mentors:** Jason Kridner, Drew Fustini, Hunyue Yau, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2020/projects/6248850903269376
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2020_Projects/Cape_Compatibility
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Parallel Bidirectional Bus for Beaglebone PRU
**********************************************

.. youtube:: cKxx8r4FRqY
   :width: 100%

| **Summary:** While the BeagleBone family has a large number of pins available on their P8/P9 headers, projects requiring a large amount of GPIO I/O may still be infeasible due to pinmux conflicts with other interfaces (SPI, UARTs, etc.). The newer PocketBeagle platform is even more restricted in the number of available GPIOs for interfacing.

**Contributor:** Deepankar Maithani

**Mentors:** Saketh, Abhishek Kumar, Jason Kridner, Hunyue Yau, Drew Fustini, rma

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2020/projects/6635395644653568
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2020_Projects/PRU_Bi-dir_bus
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Media IP Streaming
******************

.. youtube:: YOUR_YOUTUBE_VIDEO_ID
   :width: 100%

| **Summary:** This project will equip the Beagleboard AI with Media IP Streaming capabilities, by porting the sound card drivers for `CTAG face2|4 Audio Card <http://www.creative-technologies.de/linux-based-low-latency-multichannel-audio-system-2/>`_ and the AVB protocol stack from BeagleBone AVB to the BeagleBone AI.

**Contributor:** Niklas Wantrupp

**Mentors:** Robert Manzke, Henrik Langer, Drew Fustini, Indumathi Duraipandian

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2020/projects/6143660611076096
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2020_Projects/Media_IP_Streaming
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 

   Checkout eLinux page for GSoC 2020 projects `here <https://elinux.org/BeagleBoard/GSoC/2020_Projects>`_ for more details.
