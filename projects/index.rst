.. _Past_Projects:

Past Projects
##############

An overview of the significant contributions made to BeagleBoard.org through 
GSoC over the previous years is given in the section that follows. 

.. youtube:: Dk0KhYNS1CU
   :align: center
   :width: 100%

.. grid:: 2 4 4 4
    :margin: 4 4 0 0 
    :gutter: 4

    .. grid-item-card:: :far:`calendar-days` 2024
        :text-align: center
        :link: gsoc-2024-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2023
        :text-align: center
        :link: gsoc-2023-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2022
        :text-align: center
        :link: gsoc-2022-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2021
        :text-align: center
        :link: gsoc-2021-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2020
        :text-align: center
        :link: gsoc-2020-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2019
        :text-align: center
        :link: gsoc-2019-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2018
        :text-align: center
        :link: gsoc-2018-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2017
        :text-align: center
        :link: gsoc-2017-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2016
        :text-align: center
        :link: gsoc-2016-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2015
        :text-align: center
        :link: gsoc-2015-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2014
        :text-align: center
        :link: gsoc-2014-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2013
        :text-align: center
        :link: gsoc-2013-projects
        :link-type: ref

    .. grid-item-card:: :far:`calendar-days` 2010
        :text-align: center
        :link: gsoc-2010-projects
        :link-type: ref

.. admonition:: Explore!

    To further explore the contributions made to BeagleBoard.org, you can also visit all the contributions compiled in this `OpenSource Project <https://www.gsocorganizations.dev/organization/beagleboard.org/>`_ 
    by `Nishant Mittal <https://github.com/nishantwrp>`_.

.. toctree:: 
    :maxdepth: 1
    :hidden:

    2024
    2023
    2022
    2021
    2020
    2019
    2018
    2017
    2016
    2015
    2014
    2013
    2010
