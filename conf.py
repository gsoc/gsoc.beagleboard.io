# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys
from pathlib import Path
from typing import Any, Dict
from os import walk
import pydata_sphinx_theme
from sphinx.application import Sphinx

sys.path.append(str(Path(".").resolve()))

project = 'gsoc.beagleboard.io'
copyright = '2025, BeagleBoard.org'
author = 'BeagleBoard.org'

# Add epilog details to rst_epilog
rst_epilog =""
rst_epilog_path = "_static/epilog/"
for (dirpath, dirnames, filenames) in os.walk(rst_epilog_path):
    for filename in filenames:
        with open(dirpath + filename) as f:
            rst_epilog += f.read()

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx_design",
    "sphinxcontrib.youtube",
    "sphinxcontrib.images",
    "sphinx_copybutton"
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'env', ".venv"]

pages_url = "https://gsoc.beagleboard.io"
pages_slug = "latest"

# What was here was not part of the "General configuration"
# first place I could find something was at https://pydata-sphinx-theme.readthedocs.io/en/stable/user_guide/source-buttons.html


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'pydata_sphinx_theme'
html_static_path = ['_static']
html_logo = "_static/images/logo.svg"
html_favicon = "_static/images/boris.svg"
html_sourcelink_suffix = ""
html_last_updated_fmt = ""
html_theme_path = [pydata_sphinx_theme.Path()]
html_baseurl = "gsoc.beagleboard.io"
html_css_files = [
    'css/custom.css',
]

# Pages entry without primary (left) sidebar

html_sidebars = {
    "**": ["sidebar-nav-bs", "mission"],
    "ideas/index": [],
    "index": []
}

html_theme_options = {
    "external_links": [
        {
            "url": "https://summerofcode.withgoogle.com/help",
            "name": "Help",
        },
        {
            "url": "https://summerofcode.withgoogle.com/rules",
            "name": "Rules",
        },
        {
            "url": "https://summerofcode.withgoogle.com/terms",
            "name": "Terms",
        },
    ],
    "header_links_before_dropdown": 4,
    "show_prev_next": True,
    "icon_links": [
        {
            "name": "OpenBeagle",
            "url": "https://openbeagle.org/",
            "icon": "fa-brands fa-gitlab",
            "attributes": {"target": "_blank"},
        },
        {
            "name": "Docs",
            "url": "https://docs.beagleboard.org/",
            "icon": "fa-solid fa-book",
            "attributes": {"target": "_blank"},
        },
        {
            "name": "Discord",
            "url": "https://bbb.io/gsocchat",
            "icon": "fa-brands fa-discord",
            "attributes": {"target": "_blank"},
        },
        {
            "name": "Forum",
            "url": "https://bbb.io/gsocml",
            "icon": "fa-brands fa-discourse",
            "attributes": {"target": "_blank"},
        },
        {
            "name": "BeagleBoard.org",
            "url": "https://www.beagleboard.org",
            "icon": "_static/images/boris.svg",
            "type": "local",
            "attributes": {"target": "_blank"},
        },
        {
            "name": "GSoC",
            "url": "https://summerofcode.withgoogle.com/",
            "icon": "_static/images/gsoc.svg",
            "type": "local",
            "attributes": {"target": "_blank"},
        },
    ],
    "use_edit_page_button": True,
    "show_toc_level": 1,
    "navbar_align": "right",
    "show_nav_level": 1,
    "announcement": "Welcome to the site for BeagleBoard.org GSoC 2025 projects!",
    # "show_version_warning_banner": True,
    "navbar_center": ["navbar-nav"],
    "navbar_start": ["navbar-logo"],
    "navbar_end": ["theme-switcher", "navbar-icon-links"],
    # "navbar_persistent": ["search-button"],
    "footer_start": ["copyright"],
    "footer_end": ["last-updated"],
    # "content_footer_items": ["last-updated"],
    "secondary_sidebar_items": {
        "**": ["page-toc", "edit-this-page", "sourcelink","pdf","forum"]
    },
}

proposals = []
proposal_filenames = next(walk("proposals"), (None, None, []))[2]
for proposal_filename in proposal_filenames:
    proposal_filename = os.path.splitext(proposal_filename)[0]
    if proposal_filename != "index":
        proposals.append(proposal_filename)

print(proposals)

html_context = {
    "display_gitlab": True,
    "gitlab_url": "https://openbeagle.org",
    "gitlab_user": "gsoc",
    "gitlab_repo": "gsoc.beagleboard.io",
    "gitlab_version": "main",
    "doc_path": "",
    "conf_py_path": "",
    "show_license": True,
    "pages_url": "https://gsoc.beagleboard.io",
    "pages_slug": "",
    "docs_url": "https://docs.beagleboard.org/latest/",
    "edit_page_url_template": "{{ my_vcs_site }}{{ file_name }}",
    "edit_page_provider_name": "OpenBeagle",
    "my_vcs_site": "https://openbeagle.org/gsoc/gsoc.beagleboard.io/-/edit/main/",
    "proposals":proposals
}

# -- Options for proposal PDF generation --
latex_elements = {
    "papersize": "a4paper",
    "maketitle": open("_static/latex/title.tex").read(),
    "preamble": open("_static/latex/preamble.tex").read() + r"\let\cleardoublepage\clearpage",
    "sphinxsetup": ",".join(
        (
            "verbatimwithframe=false",
            "VerbatimColor={HTML}{f0f2f4}",
            "InnerLinkColor={HTML}{2980b9}",
            "warningBgColor={HTML}{e9a499}",
            "warningborder=0pt",
            r"HeaderFamily=\rmfamily\bfseries",
        )
    ),
}

sd_fontawesome_latex = True
latex_engine = "xelatex"
latex_logo = str("_static/images/logo-latex.pdf")
latex_documents = []
for proposal in proposals:
    latex_documents.append(("proposals/"+proposal, proposal+".tex", "",author, "manual"))
