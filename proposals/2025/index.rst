.. _gsoc-2025-projects:

:far:`calendar-days` 2025
##########################

Start writing your proposal for GSoC 2025 today and add it here!

.. toctree::
    :maxdepth: 1
