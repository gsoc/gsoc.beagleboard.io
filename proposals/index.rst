.. _gsoc-proposals:

Proposals
#########

.. tip:: 

    Checkout :ref:`gsoc-project-ideas` page to explore ideas and :ref:`gsoc-proposal-guide` page to write your own proposal.

.. toctree:: 
    :maxdepth: 1
    :caption: Current proposals

    2025/index

.. toctree:: 
    :maxdepth: 1
    :caption: Proposal template

    template

.. toctree::
    :maxdepth: 1
    :caption: Past Proposals

    2024/index