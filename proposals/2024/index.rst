.. _gsoc-2024-proposals:

:far:`calendar-days` 2024
##########################

.. toctree::
    :maxdepth: 1
    
    ijc/index
    aryan_nanda/index
    roger18/index
    melta101/index