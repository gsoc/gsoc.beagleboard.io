.. _gsoc-contributor-guide:

Contributor Guide 
#################

.. youtube:: YN7uGCg5vLg
    :width: 100%
    :align: center

.. important::

   Contributors will be expected to execute a series of prerequisites to demonstrate and
   expand familiarity with embedded systems development. Don't worry, the
   `live chat <https://bbb.io/gsocchat>`_ channel has over 1,000 active members to
   travel with you on your journey.

General requirements
********************

.. note::

   Google Summer of Code is open to individuals age 18 and older in most countries who are new or beginner
   contributors to open source coding projects. Read more on the GSoC site
   `Rules page <https://summerofcode.withgoogle.com/rules>`_ and the
   `FAQ page <https://developers.google.com/open-source/gsoc/faq>`_.

All projects have the following basic requirements:

1. Contributors must create accounts on our `OpenBeagle <https://openbeagle.org/>`_, `Discord <https://bbb.io/gsocchat>`_ and `Beagle Forum <http://bbb.io/gsocml>`_ prior to creating their application.
2. All newly generated materials must be released under an `open source license <http://www.opensource.org/licenses>`_. Individual contributors shall retain copyright on their works.
3. Contributors will demonstrate their ability to cross-compile and utilize version control software by creating a “Hello World” application and generating a pull request to `jadonk/gsoc-application <https://github.com/jadonk/gsoc-application/tree/master/ExampleEntryJasonKridner>`_. For assistance, please visit our `Discord <https://bbb.io/gsocchat>`_ or utilize the `Beagle Forum <http://bbb.io/gsocml>`_. The “Hello World” application must print your name and the date out in an ARM Linux environment. Freely available emulators may be used to test your application or you can ask anyone on the chat or mailing list to help you test.
4. All projects will produce reusable software components and will not be “what-I-built-over-my-summer-vacation” projects. Including a hardware component is welcome, but the project deliverable will be software that may be utilized by a wide audience of the `BeagleBoard.org <https://www.beagleboard.org/>`_ community.
5. Contributors will demonstrate their ability to collaborate by creating a project proposal on this site using our :ref:`proposal template <gsoc-proposal-template>` and utilizing our `Discord <https://bbb.io/gsocchat>`_ to collect quotes regarding the usefulness of their proposal to the `BeagleBoard.org <https://www.beagleboard.org/>`_ community. **Use of Google Docs for proposal development is discouraged due to insufficient revision control and extensive use of computing resources having numerous documents open simultaneously**.
6. Source code generated during the project must be released on `OpenBeagle <https://openbeagle.org/>`_ (and we’ll setup a mirror on `Github.com <http://github.com/>`_).
7. Contributors will provide two recorded audio/video presentations uploaded to Youtube or Vimeo (screencasts are appropriate), one near the beginning of the project summarizing their project goals and another in the wrap-up phase to summarize their accomplishments. Examples can be found in :ref:`Past_Projects`. There is no requirement to show your faces or use English.

.. important:: 

    To help you to break your project down into manageable chunks and also to help the project’s mentors to better support 
    your efforts, weekly project status reports should be e-mailed to the the `Beagle Forum <http://bbb.io/gsocml>`_. 
    Each status report should be posted by every Monday and must include these outlines:

    1. Accomplishments
    2. Resolutions to blockers
    3. On-going blockers
    4. Plans for this week

    For example, `Weekly Progress Report Thread: 
    Cape Compatibility <https://forum.beagleboard.org/t/weekly-progress-report-thread-cape-compatibility/28709/9>`_

Contributor proposals can encompass projects based on the :ref:`ideas page <gsoc-project-ideas>`  or can include personal project ideas, 
but should be focused on generating a sustainable and reusable open source software component. Previous Google Summer of Code projects show 
that the key to success is being passionate about your project, so propose something that is extremely interesting to you, even 
if it is not on this list. We will be glad to help contributors develop ideas into projects via our `Discord <https://bbb.io/gsocchat>`_ 
and `Forum <https://bbb.io/gsocml>`_. There are many potential project ideas and we will match contributors to projects 
based on their interests and help scope the proposals to something that can be completed in the Summer of Code timeframe.

.. tip::
   There are more than a thousand existing projects listed at `Projects Archive <http://www.beagleboard.org/projects>`_. 
   If you are interested in any of the projects listed on the `BeagleBoard.org <https://www.beagleboard.org/>`_ projects page, 
   contact the project members to see if there are any aspects of their projects that can be enhanced to create a GSoC project. 
   There are also several ideas on the `ECE497 class project idea list <https://elinux.org/ECE497_Project_Ideas>`_. 
   Note, however, we are looking for projects that provide a service to the developer community and aren’t simply “look what cool 
   thing I made or you can make”. The projects should provide a useful software component for other people to utilize.

